-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 20-06-2023 a las 21:13:05
-- Versión del servidor: 8.0.31
-- Versión de PHP: 8.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `frle_fedex`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

DROP TABLE IF EXISTS `cliente`;
CREATE TABLE IF NOT EXISTS `cliente` (
  `id_cli` int NOT NULL AUTO_INCREMENT,
  `apellidos_cli` varchar(30) COLLATE utf8mb4_general_ci NOT NULL,
  `nombres_cli` varchar(30) COLLATE utf8mb4_general_ci NOT NULL,
  `ubicacion_cli` varchar(500) COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id_cli`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cli`, `apellidos_cli`, `nombres_cli`, `ubicacion_cli`) VALUES
(3, 'Capito', 'Elmas', '{\"lat\":-1.062446991608149,\"lng\":-77.22759245312501}'),
(2, 'Celsiuz', 'Quelbin', '{\"lat\":-0.9745702614863846,\"lng\":-79.46880339062501}'),
(4, 'Demi', 'Ana Busado', '{\"lat\":-1.8202582491727615,\"lng\":-80.39165495312501}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido`
--

DROP TABLE IF EXISTS `pedido`;
CREATE TABLE IF NOT EXISTS `pedido` (
  `id_ped` int NOT NULL AUTO_INCREMENT,
  `descripcion_ped` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `ubicacion_ped` varchar(500) COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id_ped`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `pedido`
--

INSERT INTO `pedido` (`id_ped`, `descripcion_ped`, `ubicacion_ped`) VALUES
(3, 'Pedido 2', '{\"lat\":-1.0514625327947427,\"lng\":-76.96392057812501}'),
(2, 'Pedido 1', '{\"lat\":-1.7982965472792094,\"lng\":-76.29375456250001}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursal`
--

DROP TABLE IF EXISTS `sucursal`;
CREATE TABLE IF NOT EXISTS `sucursal` (
  `id_suc` int NOT NULL AUTO_INCREMENT,
  `nombre_suc` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `ubicacion_suc` varchar(500) COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id_suc`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `sucursal`
--

INSERT INTO `sucursal` (`id_suc`, `nombre_suc`, `ubicacion_suc`) VALUES
(3, 'Sucursal Quito', '{\"lat\":-0.1616287633257344,\"lng\":-78.45806120312501}'),
(2, 'Sucursal Ambato', '{\"lat\":-1.2381927468333858,\"lng\":-78.61186979687501}'),
(7, 'Sucursal Guayakill', '{\"lat\":-2.22649808924505,\"lng\":-79.89727018750001}'),
(8, 'Sucursal Manta', '{\"lat\":-0.9635855047694155,\"lng\":-80.69927214062501}'),
(9, 'Sucursal Cuenca', '{\"lat\":-2.895990801981768,\"lng\":-78.97441862500001}'),
(10, 'Sucursal Salinas', '{\"lat\":-2.2594318216134437,\"lng\":-80.91899870312501}');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
