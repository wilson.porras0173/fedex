<hr>
<footer class="d-grid mx-auto w-50">
    <a href="<?php echo site_url('welcome') ?>" class="btn btn-danger btn-lg">
        🔙 Volver</a>
</footer>
<script>
    //read flashdata and alert
    <?php if ($this->session->flashdata('mensaje')): ?>
        alert('<?php echo $this->session->flashdata('mensaje'); ?>');
    <?php endif; ?>
</script>
</body>

</html>