<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz"
        crossorigin="anonymous"></script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBjkNVFrdBOcMli96OZI_1X9zN_4raXvPo&libraries=places&callback=initMap"></script>

    <title>Fedex | Sucursales</title>
</head>

<body style="background-color: #e8f8f5;">
    <header>
        <div class="text-center pt-5" style="background-color: #e8f8f5;">
            <img src="<?php echo base_url('assets/fedex.png') ?>" alt="logo fedex" width="420">
        </div>
        <h1 class="text-center fs-1 fw-bold my-5" style="background-color: #e8f8f5;">Control geográfico <br> de <span
                class="text-primary">sucursales</span>, <span class="text-warning">clientes</span> y <span
                class="text-danger">pedidos</span></h1>
        <hr>
    </header>
